# How to store secrets in Git

## Tools

- [**Git-Crypt**](https://github.com/AGWA/git-crypt) Transparent encryption.
- [Git-Secret](https://git-secret.io/) Bash tool.
- [Sealed-Secret](https://github.com/bitnami-labs/sealed-secrets) Encrypt K8S `Secret` to `SealedSecret`

---

## Git-Crypt

1. Configure repository to use `git-crypt`

    ```sh
    git-crypt init
    ```

2. Specifify files to encrypt by creating `.gitattributes` file

    ```sh
    cat <<EOF > .gitattributes

    ## Prevent encrypt .gitattributes
    .gitattributes !filter !diff

    ## List of files to be encrypted
    **/.secrets/** filter=git-crypt diff=git-crypt

    EOF
    ```

3. Add user's gpg key to allow user to be decrypt.

    ```sh
    git-crypt add-gpg-user chitchaic@gmail.com
    git-crypt add-gpg-user jutinant@gmail.com
    git-crypt add-gpg-user thisadee_pre@truecorp.co.th
    ````

4. Commit and push to Git providers (eg. Gitlab, Github)
